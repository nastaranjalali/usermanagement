package main

import (
	"grpc-e2/server"
)

func main(){
	
	server.InitTables(server.Getdb())
	server.InitServer()
}