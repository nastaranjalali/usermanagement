package server

import (
	"context"
	"grpc-e2/usermanagementpb"
)

func (s *Server) Delete(ctx context.Context, req *usermanagementpb.DeleteRequest) (*usermanagementpb.DeleteResponse, error) {
	
	return &usermanagementpb.DeleteResponse{Message: "successfuly deleted!"}, nil
}
