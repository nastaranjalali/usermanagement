package server

import (
	"grpc-e2/usermanagementpb"
	"log"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Server struct {
	usermanagementpb.UnimplementedUserManagementServiceServer
}

  var db *gorm.DB
func Getdb() (db *gorm.DB){
	if db != nil{
		return db
	}
	dsn := "host=localhost user=postgres password=123456789 dbname=users port=5432 sslmode=disable TimeZone=Asia/Tehran"
	var err error
db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
if err != nil {
	log.Fatalf("failed to connect to db: %v", err)
}
return db


// fmt.Println(user.ID)             // returns inserted data's primary key
// fmt.Println(result.Error)        // returns error
// 	fmt.Println(result.RowsAffected) // returns inserted records count
}

func InitTables(db *gorm.DB){

	db.AutoMigrate(&User{})

}
func InitServer(){
	lis, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := Server{}

	grpcServer := grpc.NewServer()

	usermanagementpb.RegisterUserManagementServiceServer(grpcServer, &s)

	reflection.Register(grpcServer)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}