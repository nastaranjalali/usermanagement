package server

import (
	"context"
	"errors"
	"fmt"
	"grpc-e2/usermanagementpb"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

func (s *Server) SignIn(ctx context.Context, req *usermanagementpb.SignInRequest) (*usermanagementpb.SignInResponse, error) {
	db:=Getdb()
	var authuser User
	db.Where("username = ?", req.Username).First(&authuser)
	if authuser.Username == "" {
		err := errors.New("username or password is incorrect")
		return nil, err
	}
	check := CheckPasswordHash(req.Password, authuser.Password)

	if !check {
		err := errors.New("username or password is incorrect")
		return nil, err
	}

	validToken, err := GenerateJWT(authuser.Username, authuser.Name)
	if err != nil {
		err := errors.New("failed to generate token")
		return nil, err
	}

	fmt.Println(validToken)


	return &usermanagementpb.SignInResponse{Message: "successfuly signed in!",Token: validToken}, nil
}
func GenerateJWT(username, name string) (string, error) {
	var mySigningKey = []byte("64")
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["username"] = username
	claims["name"] = name
	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()

	tokenString, err := token.SignedString(mySigningKey)



	if err != nil {
		fmt.Errorf("Something Went Wrong: %s", err.Error())
		return "", err
	}
	return tokenString, nil
}
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
  }