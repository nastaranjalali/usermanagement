package server

import (
	"context"
	"errors"
	"grpc-e2/usermanagementpb"
	"log"

	"github.com/go-playground/validator"
	passwordvalidator "github.com/wagslane/go-password-validator"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)
var validate *validator.Validate
type User struct {
	gorm.Model
	Username 	string `gorm:"primaryKey,unique" validate:"required"`
	Password 	string `validate:"required"`
	Name         string
	Age          uint8
	
  }
func (s *Server) SignUp(ctx context.Context, req *usermanagementpb.SignUpRequest) (*usermanagementpb.SignUpResponse, error) {

	var dbuser User
	Getdb().Where("username = ?", req.Username).First(&dbuser)

	//checks if email is already register or not
	if dbuser.Username != "" {
		err := errors.New("username already in use")
				return nil, err

	}
    const minEntropyBits = 60
    err := passwordvalidator.Validate(req.Password, minEntropyBits)
	if err != nil {
		return nil, err
	}
	passwordhash, err := GeneratehashPassword(req.Password)
	if err != nil {
		log.Fatalln("error in password hash")
	}
	validate = validator.New()

	user := User{Name: req.Name, Age: uint8(req.Age),Username: req.Username, Password: passwordhash}
	err = validate.Struct(user)
 	if err != nil {

		return nil, err}

	
result := Getdb().Create(&user) // pass pointer of data to Create
	if(result.Error == nil){
			return &usermanagementpb.SignUpResponse{Message: "successfuly signed up!"}, nil

	}else{
		return nil, result.Error
	}
}

func GeneratehashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}